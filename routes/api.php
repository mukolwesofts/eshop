<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['before' => 'auth'], function () {

});

Route::group(['middleware' => 'auth:api', 'namespace'=>'Api'], function(){
    // api
});
