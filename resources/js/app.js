require('./bootstrap');

import router from './router';
import Vue from 'vue';
import store from './store/index';
import moment from 'moment';


Vue.component('example-component', require('./components/ExampleComponent.vue').default);


const app = new Vue({
    el: '#eshop',
    router,
    store,
    moment
});
