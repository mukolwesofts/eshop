
const state = {
    defaultHeaders: {}
};

const getters = {
    defaultHeaders: state => state.defaultHeaders
};

const mutations = {
    SET_DEFAULT_HEADERS(state, headers) {
        state.defaultHeaders = headers;
    }
};

export default {
    state,
    getters,
    mutations,
    modules: {

    }
}
