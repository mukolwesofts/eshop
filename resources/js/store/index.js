import Vue from 'vue';
import Vuex from 'vuex';

// import module stores
import Settings from './settings'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        Settings
    }
});


export default store;
