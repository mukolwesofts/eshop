<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'E-Shop') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Dosis|Raleway" rel="stylesheet">


    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="navbar-fixed">
            <ul id="login_dropdown" class="dropdown-content">
                <li><a href="{{ route('admin.auth.login') }}">Admin</a></li>
                <li class="divider"></li>
                <li><a href="{{ route('login') }}">Client</a></li>
            </ul>

            <nav>
                <div class="container">
                    <div class="nav-wrapper">
                        <a href="{{ url('/home') }}" class="brand-logo">E-SHOP</a>
                        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                        <ul class="right hide-on-med-and-down">
                            @auth
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                        LOGOUT
                                    </a>
                                </li>
                            @else
                                <li><a class="dropdown-trigger" href="#" data-target="login_dropdown">LOGIN</a></li>
                                <li><a href="{{ route('register') }}">REGISTER</a></li>
                            @endauth
                        </ul>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
                <li><a href="#">Sass</a></li>
                <li><a href="#">Components</a></li>
                <li><a href="#">Javascript</a></li>
                <li><a href="#">Mobile</a></li>
            </ul>
        </div>
    @endif

    <div id="eshop">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</div>
</body>
</html>
