<?php

namespace App\Http\Controllers\Auth;

use App\Eshop\Helpers\UsernameGenerator;
use App\Eshop\Models\Clients;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    use RegistersUsers;

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:clients'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    public function showRegistrationForm()
    {
        return view('client.auth.register');
    }

    protected function create(array $data)
    {
        // create user
        $user = new User();
        $data['username'] = $this->generateUsername($data['firstname'], $data['lastname'], $data['email']);
        $data['password'] = Hash::make($data['password']);
        $user->fill($data);
        $user->save();

        // create client
        $client = new Clients();
        $data['user_id'] = $user->id;
        $client->fill($data);
        $client->save();

        return $user;
    }

    private function generateUsername($firstname, $lastname, $email)
    {
        $try = 0;

        while (true) {
            $username = UsernameGenerator::generate($firstname, $lastname, '', $email, $try);

            $isAvailable = !User::where('username', $username)->exists();
            $isLong = strlen($username) >= 4;

            if ($isAvailable && $isLong) {
                break;
            }

            $try++;
        }

        return $username;
    }
}
