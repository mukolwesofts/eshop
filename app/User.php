<?php

namespace App;

use App\Eshop\Helpers\Traits\Uuids;
use App\Eshop\Models\UserType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, Uuids, SoftDeletes;

    protected $fillable = [
        'username', 'password', 'user_type', 'uuid'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function type()
    {
        return $this->belongsTo(UserType::class, 'user_type');
    }

    public function scopeOfType($query, $slug)
    {
        return $query->whereHas(
            'type',
            function ($type) use ($slug) {
                $type->where('slug', $slug);
            }
        );
    }
}
