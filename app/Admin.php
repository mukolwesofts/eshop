<?php

namespace App;

use App\Eshop\Helpers\Traits\Uuids;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Admin extends Authenticatable
{
    use Notifiable, HasApiTokens, Uuids, SoftDeletes;

    protected $fillable = [
        'name', 'phone', 'email', 'uuid', 'user_id'
    ];

    protected $table = 'admins';

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

