<?php

namespace App\Eshop\Models;

use App\Eshop\Helpers\Traits\Uuids;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clients extends Model
{
    use SoftDeletes, Uuids;

    protected $table = 'clients';

    protected $fillable = [
        'firstname', 'lastname', 'phone', 'email', 'user_id'
    ];

    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}